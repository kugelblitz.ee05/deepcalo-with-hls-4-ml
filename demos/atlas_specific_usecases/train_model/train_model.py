import os
import sys
import copy
import h5py
import numpy as np
import argparse
import pickle
import deepcalo as dpcal

# ==============================================================================
# Argument parsing
# ==============================================================================
parser = argparse.ArgumentParser()
parser.add_argument('-g','--gpu', help='Which GPU(s) to use, e.g. "0" or "1,3".', default='', type=str)
parser.add_argument('--data_path', help='Path to data. Can be relative.', default=None, type=str)
parser.add_argument('--exp_dir', help='Directory of experiment, e.g. "my_exp/".', default='./', type=str)
parser.add_argument('--n_train', help='How many data points to load from the training set. Defaults to -1 to use the value given in params_conf.py.', default=-1, type=float)
parser.add_argument('--n_val', help='How many data points to load from the validation set. Defaults to -1 to use the value given in params_conf.py.', default=-1, type=float)
parser.add_argument('--n_test', help='How many data points to load from the test set. Defaults to -1 to use the value given in params_conf.py.', default=-1, type=float)
parser.add_argument('--rm_bad_reco', help='Removes all points that are badly reconstructed by the current energy calibration.', default=False, type=dpcal.utils.str2bool)
parser.add_argument('--zee_only', help='Uses only Z->ee data.', default=False, type=dpcal.utils.str2bool)
parser.add_argument('--lh_cut_name', help='Which, if any, likehood cut to apply. Should be the name of the variable that should be True for that point to not be masked out. E.g., for electrons, use "p_LHLoose", while for photons, use "p_photonIsLooseEM" (or "Medium" or "Tight"). None (default) will not apply any likehood cut.', default=None, type=str)
parser.add_argument('--rm_conv', help='Mask out converted photons.', default=None, type=dpcal.utils.str2bool)
parser.add_argument('--rm_unconv', help='Mask out unconverted photons.', default=None, type=dpcal.utils.str2bool)
parser.add_argument('--save_figs', help='Save figures.', default=True, type=dpcal.utils.str2bool)
parser.add_argument('-v','--verbose', help='Verbose output, 0, 1 or 2, where 2 is less verbose than 1.', default=2, type=int)
args = parser.parse_args()

gpu_ids = args.gpu
data_path = args.data_path
exp_dir = args.exp_dir
_n_train = int(args.n_train)
_n_val = int(args.n_val)
_n_test = int(args.n_test)
save_figs = args.save_figs
rm_bad_reco = args.rm_bad_reco
zee_only = args.zee_only
lh_cut_name = args.lh_cut_name
rm_conv = args.rm_conv
rm_unconv = args.rm_unconv
verbose = args.verbose
apply_mask = rm_bad_reco or zee_only or lh_cut_name or rm_conv or rm_unconv

# Set which GPU(s) to use
os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
os.environ['CUDA_VISIBLE_DEVICES'] = gpu_ids

# ==============================================================================
# Get hyperparameters
# ==============================================================================
# Import parameter configurations that should be different from the default as a module
from importlib import import_module
param_conf = import_module(exp_dir.split('/')[-2] + '.param_conf')
params = param_conf.get_params()

# Use the chosen parameters where given, and use the default parameters otherwise
params = dpcal.utils.merge_dicts(dpcal.utils.get_default_params(), params, in_depth=True)

# Set learning rate based on optimizer and batch size
if params['auto_lr']:
    params = dpcal.utils.set_auto_lr(params)

# Integrate the hyperparameters given from the command line
params['n_gpus'] = len(gpu_ids.replace(',',''))

# ==============================================================================
# Logging directories
# ==============================================================================
# Make directories for saving figures and models
# dirs is a dictionary of paths
dirs = dpcal.utils.create_directories(exp_dir, params['epochs'], log_prefix=gpu_ids)

# ==============================================================================
# Load data
# ==============================================================================
# Load all points (None), or the first n number of points (e.g. n_train==int(1e4))
n_points = {'train':None, 'val':None, 'test':None}
for set_name, n in zip(n_points, [_n_train, _n_val, _n_test]):
    if not n < 0:
        n_points[set_name] = n

# If a datagenerator is used, load a single point for each set (e.g. train, val
# and test) to construct the model with (the construction uses the shapes of the
# data). The actually used number of points from each set is given by
# params['data_generator']['n_points']
if params['data_generator']['use']:
    params['data_generator']['n_points'] = n_points
    n_points = {set_name:1 for set_name in n_points}
    if data_path is not None:
        params['data_generator']['path'] = data_path

    # Load data
    data = dpcal.utils.load_atlas_data(n_points=n_points,
                                       **params['data_generator']['load_kwargs'],
                                       verbose=False)
else:
    # Import data parameters (what should or should not be loaded) as a module
    data_conf = import_module(exp_dir.split('/')[-2] + '.data_conf')
    data_params = data_conf.get_params()

    # Load the data
    data = dpcal.utils.load_atlas_data(path=data_path, n_points=n_points, **data_params)

# ==============================================================================
# Apply masks
# ==============================================================================
if not params['data_generator']['use']:

    if apply_mask:
        # Make masks
        mask = dpcal.utils.make_mask(data_path, n_points, rm_bad_reco=rm_bad_reco,
                                     zee_only=zee_only, lh_cut_name=lh_cut_name,
                                     rm_conv=rm_conv, rm_unconv=rm_unconv)

        # Apply mask
        data = dpcal.utils.apply_mask(data, mask, skip_name='test') # Don't apply to the test set

        # Save mask
        with open(dirs['log'] + 'mask.pkl', 'wb+') as f:
            pickle.dump(mask,f)

# ==============================================================================
# Standardization (see dpcal.utils.set_outliers for setting outliers)
# ==============================================================================
# These variables should be standardized with the QuantileTransformer,
# instead of the RobustScaler, as they have large-tailed distributions.
scalars_to_quantile = ['p_pt_track', 'p_R12', 'p_deltaPhiRescaled2', 'p_deltaEta2',
                        'p_f0Cluster', 'p_eAccCluster', 'p_photonConversionRadius',]

# Standardize scalars and tracks
for dataset_name in ['scalars', 'tracks']:

    if dataset_name=='scalars':
        data_param_name = 'scalar_names'
    elif dataset_name=='tracks':
        data_param_name = 'track_names'
    if data_params[data_param_name] is None:
        continue

    # Make directory for saving scalers
    scaler_dir = dirs['log'] + 'scalers/'
    if not os.path.exists(scaler_dir):
        os.makedirs(scaler_dir)

    for name in data_params[data_param_name]:
        # Get index of variable
        var_ind = data_params[data_param_name].index(name)

        # Get which scaler to use
        scaler_name = 'Quantile' if name in scalars_to_quantile else 'Robust'

        # Standardize in-place
        dpcal.utils.standardize(data,
                                dataset_name,
                                variable_index=var_ind,
                                scaler_name=scaler_name,
                                save_path=os.path.join(scaler_dir, f'scaler_{scaler_name}_{name}.jbl'))

        if verbose:
            print(f'Standardizing {name} with {scaler_name}Scaler')
            for set_name in data:
                print(f'Min and max of {name} in {set_name} set after standardization: '
                      f'{data[set_name][dataset_name][:,var_ind].min(), data[set_name][dataset_name][:,var_ind].max()}')

# ==============================================================================
# Create and train model
# ==============================================================================
# Instantiate model container (with self.model in it)
mc = dpcal.ModelContainer(data=data,
                          params=params,
                          dirs=dirs,
                          save_figs=save_figs,
                          verbose=verbose)

# Save hyperparameters (with some additional information)
params_for_saving = copy.deepcopy(mc.params)
params_for_saving['n_params'] = mc.model.count_params()
dpcal.utils.save_dict(params_for_saving, dirs['log'] + 'hyperparams.txt', save_pkl=True)

# Save data parameters (with some additional information)
data_params['data_path'] = data_path
data_params['flags'] = args.__dict__
dpcal.utils.save_dict(data_params, dirs['log'] + 'dataparams.txt', save_pkl=True)

# Train model
mc.train()

# Evaluate (predicting and evaluating on test or validation set)
if not hasattr(mc,'evaluation_scores'):
    mc.evaluate()

# Print results
if verbose:
    print('Evaluation scores:')
    print(mc.evaluation_scores)
