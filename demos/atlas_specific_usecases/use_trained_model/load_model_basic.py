import os
import sys
import json
import argparse
import numpy as np
import keras as ks
import deepcalo as dpcal

# Argument parsing
parser = argparse.ArgumentParser()
parser.add_argument('--model_path', help='Path of JSON model to be loaded.', default=None, type=str)
parser.add_argument('--weights_path', help='Path of HDF5 weights to be loaded.', default=None, type=str)
parser.add_argument('--data_path', help='Path to data', default=None, type=str)
args = parser.parse_args()

model_path = args.model_path
weights_path = args.weights_path
data_path = args.data_path

# ======================================================
# Load model
# ======================================================
# Model reconstruction from JSON
with open(model_path, 'r') as model_json:
    arch = json.load(model_json)
    model = ks.models.model_from_json(arch)
print(f'Model architecture loaded from {model_path}.')

# Inspect the structure of the model
model.summary()
print('Layers in loaded model:')
for model_component in model.layers:
    print(model_component.name)

    # Unroll submodels
    if isinstance(model_component,ks.engine.training.Model):
        print(f'\tSubstructure of {model_component.name}:')
        for layer in model_component.layers:
            print('\t' + layer.name)

# Load weights
model.load_weights(weights_path)
print(f'Weights loaded from {weights_path}.')

# ======================================================
# Load data
# ======================================================
# Load the first point of the training set, no points of the validation set (e.g. 'val':10), and the first 100 points of the test set.
# Choose None instead of a number to load all points in that set (e.g. 'test':None).
n_points = {'train':1, 'test':1e2}

# Load target (here true energies)
target_name = 'p_truth_E'

# Load the ECAL images from the barrel (the possibilities being 'em_barrel', 'em_endcap', 'lar_endcap', 'tile_barrel', 'tile_gap')
# The model I shared was only trained on 'em_barrel'
img_names = ['em_barrel']

# Load some scalar names (not used by the model, only for evaluation/inspection)
scalar_names = ['p_eta', 'p_phi', 'p_calibratedE']

data = dpcal.utils.load_atlas_data(data_path, n_points=n_points,
                                   target_name=target_name, img_names=img_names,
                                   scalar_names=scalar_names,
                                   track_names=None, max_tracks=None,
                                   sample_weight_name=None, verbose=True)

# Inspect the data (uncomment to show)
# from pprint import pprint
# for set_name in data:
#     print(set_name)
#     pprint(data[set_name])

some_img = data['train']['images'][img_names[0]]
print(f'This is a single image of size {some_img.shape[1:]}:')
print(some_img)

# ======================================================
# Use the model for inference
# ======================================================
# Predict on the 100 first test datapoints (values are in GeV)
print('Predicting.')
preds_cnn = model.predict(data['test']['images'][img_names[0]][:100]).flatten()
# preds_Ecalib = data['test']['scalars'][:100,2]
truths = data['test']['targets'][:100]

# Evaluate
print(f'Mean absolute relative error for the CNN: {np.mean(np.abs( (preds_cnn - truths)/truths) )}')
# print(f'Mean absolute relative error for current energy calibration: {np.mean(np.abs( (preds_Ecalib - truths)/truths) )}')
