def get_params():
    """
    Returns a dictionary containing parameters to be passed to the loading
    function.
    """

    params = {
              'target_factor'          : 1,           #scale down the targets
              'target_name'            : 'p_truth_E',
              'img_names'              : ['em_barrel'], # NOTE: Should actually be the added barrel and endcap layers!
              'gate_img_prefix'        : None,
              'scalar_names'           : None,
              'track_names'            : None,
              'max_tracks'             : None,
              'multiply_output_name'   : None,
              'sample_weight_name'     : None,
              }

    return params
