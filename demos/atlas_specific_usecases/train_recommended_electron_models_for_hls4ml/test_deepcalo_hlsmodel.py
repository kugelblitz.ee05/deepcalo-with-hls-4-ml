import tensorflow as tf
import hls4ml
import numpy as np
from deepcalo.layers import FiLM, Slice_tensor1D,Sum1D, Mask_track




#Load Model
custom_objects = {
                'Slice_tensor1D'    :   Slice_tensor1D,
                'FiLM'              :   FiLM,
                'Sum1D'             :   Sum1D,
                'Mask_track'        :   Mask_track
                }

model = tf.keras.models.load_model(filepath = 'final_model.h5',  custom_objects = custom_objects )
model.summary()


# Plot model
# dot_img_file = 'model.png'
# tf.keras.utils.plot_model(model, to_file=dot_img_file, show_shapes=True)


# inputs
DATA_NUM = 100

# input image
x_test_em_barrel = np.load('x_test_em_barrel.npy').astype(np.float32)[:DATA_NUM]

# input scalar
x_test_scalars = np.load('x_test_scalars.npy').astype(np.float32)[:DATA_NUM]

# input track
x_test_tracks = np.load('x_test_tracks.npy').astype(np.float32)[:DATA_NUM]



#generate hls model
hls_path = 'Deepcalo_model'

config = hls4ml.utils.config_from_keras_model(model, granularity='model')
config['Model']['Strategy'] = 'Resource'
config['Model']['ReuseFactor'] = 16384
config['Model']['Precision'] = 'ap_fixed<32,16>'

hls_model = hls4ml.converters.convert_from_keras_model(model,
                                                       io_type='io_stream',
                                                       hls_config=config,
                                                       output_dir=hls_path,
                                                       part="xczu9eg-ffvb1156-2-e")
                                                       
hls_model.compile()


# plot hls model
#hls4ml.utils.plot.plot_model(hls_model, to_file='Deepcalo.png', show_shapes=True, show_layer_names=True, show_precision=True, rankdir='TB', dpi=96)



# compare the predictions
x_test = [x_test_em_barrel,x_test_scalars, x_test_tracks]

y_model = model.predict(x_test)
y_hls = hls_model.predict(x_test)

print('y_model shape: ',y_model.shape)
print('y_hls shape: ',y_hls.shape)

print('y_model: ',y_model)
print('y_hls: ',y_hls)

from sklearn.metrics import mean_absolute_percentage_error
print("Mean_absolute_percentage_error hls4ml: {}".format(mean_absolute_percentage_error(y_model,y_hls)))

from sklearn.metrics import mean_squared_error
print("Mean square error hls4ml: {}".format(mean_squared_error(y_model,y_hls)))



# write hls c++ files
hls_model.write()


#write x_test to tb_input_features.dat
INPUT_FILE = hls_path+'/tb_data/tb_input_features.dat'

import sys
original_stdout = sys.stdout
with open(INPUT_FILE, 'a') as f:
    sys.stdout = f # Change the standard output to the file 
    
    for i in range(DATA_NUM):
        x_test_em_barrel_list = x_test_em_barrel[i].reshape(-1).tolist()
        for x in x_test_em_barrel_list:
            print(x,end = ' ')
                
        x_test_scalars_list = x_test_scalars[i].reshape(-1).tolist()
        for x in x_test_scalars_list:
            print(x,end = ' ')
                
        x_test_tracks_list = x_test_tracks[i].reshape(-1).tolist()
        for x in x_test_tracks_list:
            print(x,end = ' ')
                
        print('')    
    sys.stdout = original_stdout

#write y_model to tb_output_predictions.dat
np.savetxt(hls_path+'/tb_data/tb_output_predictions.dat', y_model[:DATA_NUM].reshape(-1)) 






