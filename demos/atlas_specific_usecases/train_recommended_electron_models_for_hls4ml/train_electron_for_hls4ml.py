import os
import deepcalo as dpcal
import tensorflow as tf
import numpy as np
# ==============================================================================
# Argument, for further available arguments please refer to the readme file
# ==============================================================================

exp_dir = 'exp_electrons/'
data_path = 'MC_abseta_1.3_1.6_et_0.0_5000000.0_processes_energy.h5'
rm_bad_reco = True
zee_only = True
_n_train = 1e3
_n_val = 1e3
_n_test = 1e3
# gpu_ids = "0"
apply_mask = rm_bad_reco or zee_only 

# # Set which GPU(s) to use
# os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
# os.environ['CUDA_VISIBLE_DEVICES'] = gpu_ids


# ==============================================================================
# Get hyperparameters
# ==============================================================================
# Import parameter configurations that should be different from the default as a module
from importlib import import_module
param_conf = import_module(exp_dir.split('/')[-2] + '.param_conf')
params = param_conf.get_params()

# Use the chosen parameters where given, and use the default parameters otherwise
params = dpcal.utils.merge_dicts(dpcal.utils.get_default_params(), params, in_depth=True)

# Set learning rate based on optimizer and batch size
if params['auto_lr']:
    params = dpcal.utils.set_auto_lr(params)

# Integrate the hyperparameters given from the command line
# params['n_gpus'] = len(gpu_ids.replace(',',''))

# ==============================================================================
# Logging directories
# ==============================================================================
# Make directories for saving figures and models
# dirs is a dictionary of paths
dirs = dpcal.utils.create_directories(exp_dir, params['epochs'])



# ==============================================================================
# Load data
# ==============================================================================
# Load all points (None), or the first n number of points (e.g. n_train==int(1e4))
n_points = {'train':None, 'val':None, 'test':None}
for set_name, n in zip(n_points, [_n_train, _n_val, _n_test]):
    if not n < 0:
        n_points[set_name] = n

# If a datagenerator is used, load a single point for each set (e.g. train, val
# and test) to construct the model with (the construction uses the shapes of the
# data). The actually used number of points from each set is given by
# params['data_generator']['n_points']
if params['data_generator']['use']:
    params['data_generator']['n_points'] = n_points
    n_points = {set_name:1 for set_name in n_points}
    if data_path is not None:
        params['data_generator']['path'] = data_path

    # Load data
    data = dpcal.utils.load_atlas_data(n_points=n_points,
                                       **params['data_generator']['load_kwargs'],
                                       verbose=False)
else:
    # Import data parameters (what should or should not be loaded) as a module
    data_conf = import_module(exp_dir.split('/')[-2] + '.data_conf')
    data_params = data_conf.get_params()

    # Load the data
    data = dpcal.utils.load_atlas_data(path=data_path, n_points=n_points, use_hls4ml = params['use_hls4ml'], **data_params)


# ==============================================================================
# Save data
# ==============================================================================


set_name = 'test'

#input images
x_test_em_barrel = data[set_name]['images']['em_barrel']
np.save('x_test_em_barrel.npy', x_test_em_barrel)
print('x_test_em_barrel shape: ', x_test_em_barrel.shape)
print('x_test_em_barrel saved')

#input scalar
x_test_scalars = data[set_name]['scalars']
np.save('x_test_scalars.npy', x_test_scalars)
print('x_test_scalars shape: ', x_test_scalars.shape)
print('x_test_scalars saved')

#input track
x_test_tracks = data[set_name]['tracks']
np.save('x_test_tracks.npy', x_test_tracks)
print('x_test_tracksl shape: ', x_test_tracks.shape)
print('x_test_tracks saved')

#target
y_test_targets = data[set_name]['targets']
np.save('y_test_targets.npy', y_test_targets)
print('y_test_targets shape: ', y_test_targets.shape)
print('y_test_targets saved')


# ==============================================================================
# Create and train model
# ==============================================================================
# Instantiate model container (with self.model in it)
mc = dpcal.ModelContainer(data=data,
                          params=params,
                          dirs=dirs,
                          save_figs=False,
                          verbose=True)



model = mc.model
#dot_img_file = 'electron_train.png'
#tf.keras.utils.plot_model(model, to_file=dot_img_file, show_shapes=True)
# Train model
mc.train()

# Evaluate (predicting and evaluating on test or validation set)
if not hasattr(mc,'evaluation_scores'):
    mc.evaluate()

# Print results

print('Evaluation scores:')
print(mc.evaluation_scores)



model = mc.model
print('final model saved')
model.save('final_model.h5')



